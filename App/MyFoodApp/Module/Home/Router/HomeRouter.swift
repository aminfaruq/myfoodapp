//
//  HomeRouter.swift
//  MyFoodApp
//
//  Created by Amin faruq on 12/01/21.
//

import SwiftUI
import Category
import Core
import Meal

class HomeRouter {
    func makeMealsView(for category: CategoryModel) -> some View {
        
        let useCase: Interactor<
            String,
            [MealModel],
            GetMealsRepository<
                GetMealsLocaleDataSource,
                GetMealsRemoteDataSource,
                MealsTransformer<MealTransformer<IngredientTransformer>>>
        > = Injection.init().provideMeals()
        
        let presenter = GetListPresenter(useCase: useCase)
        
        return MealsView(presenter: presenter, category: category)
    }
}
